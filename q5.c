#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef enum menu{EXIT,ADD,FIND,DISPLAY,EDIT,DELETE}men_u;

typedef struct inventory{
    int id;
    char name[20];
    float price;
    int qty;
}ivt_ry;

void scan();
void print();

int main(void)
{
    int seek,del;
    ivt_ry arr;
    men_u choice;
    int i,found=0;
    char search[20];

    FILE *fp = fopen("inventory.txt","a+");
    FILE *fp1 = fopen("temp.txt","w");
    printf("0.EXIT\n1.ADD\n2.FIND\n3.DISPLAY\n4.EDIT\n5.DELETE\n");
    printf("Enter your choice: ");
    scanf("%d",&choice);

    switch(choice)
    {
        case EXIT:
            exit(0);
            break;

        case ADD:
            scan(&arr);
            fwrite(&arr,sizeof(ivt_ry),1,fp);
            break;

        case FIND:
            fp = fopen("inventory.txt","r");
            printf("Enter item name to be searched: ");
            scanf("%*c%[^\n]s",search);
            while(fread(&arr, sizeof(ivt_ry), 1, fp))
            {
                if(strcmp(arr.name,search)==0)
                {
                    found=1;
                    printf("%-5d %-20s %10.2f %5d\n",arr.id,arr.name,arr.price,arr.qty);
                }
            }
            if(found==0)
            printf("Item not found");
            break;

        case DISPLAY:
            fp = fopen("inventory.txt","r");
            while((fread(&arr, sizeof(ivt_ry), 1, fp)) != 0)
                print(&arr);
            fclose(fp);
            break;

        case EDIT:
            fp = fopen("inventory.txt","r+");
            printf("Enter file number which you want to edit: ");
            scanf("%d",&seek);
            if(seek==1)
                fseek(fp, 0*sizeof(ivt_ry), SEEK_CUR);
            else
                fseek(fp, seek*sizeof(ivt_ry)-sizeof(ivt_ry), SEEK_CUR);
            scan(&arr);
            fwrite(&arr,sizeof(ivt_ry),1,fp);
            break;

        case DELETE:
            printf("Enter the item id to be deleted : ");
            scanf("%d",&del);
            while(fread(&arr, sizeof(ivt_ry), 1, fp))
            {
                if(arr.id==del)
                {
                    found=1;
                }
                else
                    fwrite(&arr,sizeof(ivt_ry),1,fp1);
            }        
                fclose(fp);
                fclose(fp1);
                if(found)
                {
                    fp1 = fopen("temp.txt","r");
                    fp = fopen("inventory.txt","w");
                    while(fread(&arr, sizeof(ivt_ry), 1, fp1))
                    {
                        fwrite(&arr,sizeof(ivt_ry),1,fp);
                    }
                    fclose(fp);
                    fclose(fp1);
                }
                else
                printf("Item not found\n");
            break;
    }


    fclose(fp);
    return 0;
}

void scan(ivt_ry *arr)
{
    printf("Enter ID: ");
    scanf("%d",&arr->id);
    printf("Enter Name: ");
    scanf("%*c%[^\n]s",arr->name);
    printf("Enter Price: ");
    scanf("%f",&arr->price);
    printf("Enter Quantity: ");
    scanf("%d",&arr->qty);
}

void print(ivt_ry *arr)
{
    printf("%-5d %-20s %10.2f %5d\n",arr->id,arr->name,arr->price,arr->qty);
}

