#include<stdio.h>

struct result{
    char grade;
    float per;
};

typedef struct student{
    int rollno;
    char name[20];
    int std;
    struct result result;
}stud_t;

void accept_student();
void print_student();

int main(void)
{
    stud_t s1[36];        
    accept_student(s1);
    print_student(s1);
     
    return 0;
}
void accept_student(stud_t *s)
{
    int i;
    for(i=0;i<5;i++)
    {
        printf("Enter RollNo, name, std [student(%d)] : ",i+1);   
        scanf("%d %s %d", &s[i].rollno, s[i].name, &s[i].std);    
        if(s[i].std <= 4)                                        
        {
            printf("Enter grade : ");
            scanf("%*c%c", &s[i].result.grade); 
        }              
        else
        {
            printf("Enter percenatge : ");
            scanf("%f", &s[i].result.per);             
        }
    }
}

void print_student(stud_t *s)
{
    int i;
    for(i=0;i<5;i++)
    {
    printf("%d %s %d ", s[i].rollno, s[i].name, s[i].std);                               
    s[i].std <= 4 ? printf("%c\n", s[i].result.grade) : printf("%0.2f\n", s[i].result.per);  
    }
}
