#include<stdio.h>
#include <stdlib.h>

int** accept_array(int row,int col);
void print_array(int **arr,int row,int col);
void free_array(int **arr,int row,int col);

int** sum_array(int **arr1,int **arr2,int row,int col);
int** sub_array(int **arr1,int **arr2,int row,int col);
int** mul_array(int **arr1,int **arr2,int row,int col);

int main(void)
{
    int **arr1;
    int **arr2;
    int **sum,**sub,**mul;
    int row,col;
    printf("Enter value for row and coloum: ");
    scanf("%d %d",&row,&col);
    printf("Enter matrix-1 elements:\n");
    arr1 = accept_array(row,col);
    printf("Enter matrix-2 elements:\n");
    arr2 = accept_array(row,col);
    
    printf("Matrix: 1\n");
    print_array(arr1,row,col);
    
    printf("Matrix: 2\n");
    print_array(arr2,row,col);

    if(row!=2 && col!=2)
    {  
      sum=sum_array(arr1,arr2,row,col);
      printf("sum :\n");
      print_array(sum,row,col);
      
      sub=sub_array(arr1,arr2,row,col);
      printf("sub :\n");
      print_array(sub,row,col);
      
      mul=mul_array(arr1,arr2,row,col);
      printf("mul :\n");
      print_array(mul,row,col);
    }

    free_array(arr1,row,col);
    free_array(arr2,row,col);

    if(row!=2 & col!=2)
    {
      free_array(sum,row,col);
      free_array(sub,row,col);
      free_array(mul,row,col);
    } 
    return 0;
}
int** accept_array(int row,int col)
{
    int i,j;
    int **arr;
    arr =(int **)calloc(row , sizeof(int *));
    for(i=0;i<row;i++){
        arr[i] =(int *)calloc(col, sizeof(int));
    }

    for(i=0;i<row;i++)
    {
        for(j=0;j<col;j++)
        {
            printf("arr[%d][%d]=",i,j);
            scanf("%d",&arr[i][j]);
        }
    }
    return arr;
}

void print_array(int **arr,int row,int col)
{
    int i,j;
    
    for(i=0;i<row;i++)
    {
        for(j=0;j<col;j++)
        {
            printf("%4d ",arr[i][j]);
        }
        printf("\n");
    }

}

int** sum_array(int **arr1,int **arr2,int row,int col)
{
    int i,j;
    int **sum;

    sum =(int **)calloc(row , sizeof(int *));
    for(i=0;i<row;i++){
        sum[i] =(int *)calloc(col, sizeof(int));
    }

    for(i=0;i<row;i++)
    {
        for(j=0;j<col;j++)
        {
            sum[i][j]=arr1[i][j] + arr2[i][j];
        }
    }
    return sum;
}
int** sub_array(int **arr1,int **arr2,int row,int col)
{
    int i,j;
    int **sub;

    sub =(int **)calloc(row , sizeof(int *));
    for(i=0;i<row;i++){
        sub[i] =(int *)calloc(col, sizeof(int));
    }

    for(i=0;i<row;i++)
    {
        for(j=0;j<col;j++)
        {
            sub[i][j]=arr1[i][j] - arr2[i][j];
        }
    }
    return sub;
}

int** mul_array(int **arr1,int **arr2,int row,int col)
{
    int i,j;
    int **mul;

    mul =(int **)calloc(row , sizeof(int *));
    for(i=0;i<row;i++){
        mul[i] =(int *)calloc(col, sizeof(int));
    }

    for(i=0;i<row;i++)
    {
        for(j=0;j<col;j++)
        {
            mul[i][j]=arr1[i][j] * arr2[i][j];
        }
    }
    return mul;
}
void free_array(int **arr,int row,int col){
    int i;
    for(i = 0 ; i < row ; i++)
        free(arr[i]);

    free(arr);
   
    arr=NULL;
}
