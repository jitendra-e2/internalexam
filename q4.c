#include<stdio.h>
#include<stdlib.h>

int compare(const void *arg1,const void *arg2);


void my_qsort(int* , int ,int (*compare_ptr)(const void *,const void*));

int main()
{
    int i;
    
    int arr[5];
    for( i = 0 ; i < 5 ; i++)
    {
        printf("Enter arr[%d] : ", i);
        scanf("%d", &arr[i]);
    }

    for( i = 0 ; i < 5 ; i++)
        printf("arr[%d] = %d\n", i, arr[i]);
    printf("Array before sorting\n");
    for ( i = 0; i < 5; i++)
    {
        printf("%d\t",arr[i]);
    }
    
    int (*compare_ptr)(const void* ,const void*);

    compare_ptr = compare;

    my_qsort(arr,5,compare_ptr);
    printf("\nSorted array is\n");
   for ( i = 0; i < 5; i++)
   {
     printf("%d\t",arr[i]);
   }
    return 0;

}
void my_qsort(int*arr , int elements, int (*compare_ptr)(const void *,const void*))
{
     for (int  i = 0; i < elements-1; i++)
     {
         int max = 0;
         int swap = 0;
         for (int  j = i+1; j < elements; j++)
         {
                 max = compare_ptr(arr+i,arr+j);

                 if(max > 0)
                 {
                  swap = *(arr+i);
                 *(arr+i) = *(arr+j);
                 *(arr+j) = swap;
                 }
                
         }
    }     
}
int compare( const void *arg1, const void *arg2)
{
    return *(int *)arg1 - *(int *)arg2;
}


